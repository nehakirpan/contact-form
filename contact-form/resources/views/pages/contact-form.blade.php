@extends('welcome')

@section('title')
    
@endsection()

@section('css')
    @vite(['resources/sass/contact-form.scss'])
@endsection()

@section('content')
<main>
    <section class="container">
        <h1>Contact Form</h1>
        <form class="form-inputs" name="myForm">
            <div>
                <label>Name:</label>
                <input class="form-control" type="text" placeholder="Name" name="fname" required>
            </div>
           <div>
                <label>Email:</label>
                <input class="form-control" type="email" placeholder="email" required>
           </div>
           <div>
                <label>Phone:</label>
                <input class="form-control" type="phone" placeholder="Phone no" required>
           </div>
            <div>
                <label>Address:</label>
                <input class="form-control" type="text" placeholder="Address" required>
            </div>
           <div>
           <label>State:</label>
            <select class="form-select" aria-label="Default select example" required>
                <option selected>Select State</option>
                <option value="1">Maharashtra</option>
                <option value="2">Karnataka</option>
                <option value="3">panjab</option>
            </select>
           </div>
            <div>
                <Label>Select Date:</Label>
                <input class="form-control" type="date" required>
            </div>
           
           <div>
           <label>Select Gender</label>
           <div class="form-check">
                
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                <label class="form-check-label" for="flexRadioDefault1">
                    Male
                </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                <label class="form-check-label" for="flexRadioDefault2">
                    Female
                </label>
            </div>

           </div>
           <div>
                <label>Choose File:</label>
                <div class="mb-3">
                    <input class="form-control" type="file" id="formFile" required>
                </div>
            </div>
            <div>
                <label for="exampleColorInput" class="form-label">choose color</label>
                <input type="color" class="form-control form-control-color" id="exampleColorInput" value="#563d7c" title="Choose your color">
           </div>
           <div>
           <input type="submit" class="btn btn-outline-secondary mt-4 mb-5" value="Submit" onclick="validateForm()">
           </div>
        </form>
    </section>
</main>

@endsection()

@section('javascript')
@vite(['resources/js/contact-form.js'])
@endsection()