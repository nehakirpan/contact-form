import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css','resources/sass/contact-form.scss','resources/js/contact-form.js', 'resources/js/app.js'],
            refresh: true,
        }),
    ],
});
